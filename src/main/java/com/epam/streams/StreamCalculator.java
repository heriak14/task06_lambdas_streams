package com.epam.streams;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamCalculator {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final Random RANDOM = new Random();

    private List<Integer> getUsingGenerate() {
        return Stream.generate(() -> RANDOM.nextInt(21)).limit(10).collect(Collectors.toList());
    }

    private List<Integer> getUsingIterate() {
        return Stream.iterate(0, n -> n + RANDOM.nextInt(10)).limit(10).collect(Collectors.toList());
    }

    private Integer[] getUsingBuilder() {
        Stream.Builder<Integer> builder = Stream.builder();
        for (int i = 0; i < 10; i++) {
            builder.add(RANDOM.nextInt(20));
        }
        return builder.build().toArray(Integer[]::new);
    }

    private void printAverage() {
        List<Integer> usingGenerate = getUsingGenerate();
        LOGGER.info("For ");
        usingGenerate.forEach(e -> LOGGER.info(e + " "));
        OptionalDouble avg = usingGenerate.stream()
                .mapToInt(Integer::intValue)
                .average();
        LOGGER.info("\nAverage = " + avg.getAsDouble());
        LOGGER.info("\nNumbers bigger than average: ");
        usingGenerate.stream().filter(n -> n > avg.getAsDouble()).forEach(n -> LOGGER.info(n + " "));
    }

    private void printSum() {
        List<Integer> usingIterate = getUsingIterate();
        LOGGER.info("\nFor ");
        usingIterate.forEach(e -> LOGGER.info(e + " "));
        Integer sum1 = usingIterate.stream()
                .mapToInt(Integer::intValue)
                .sum();
        LOGGER.info("\nSum using sum() = " + sum1);
        Integer sum2 = usingIterate.stream()
                .reduce(0, (a, b) -> a + b);
        LOGGER.info("\nSum using reduce() = " + sum2);
    }

    private void printMinMax() {
        Integer[] usingBuilder = getUsingBuilder();
        LOGGER.info("\nFor ");
        Arrays.stream(usingBuilder).forEach(e -> LOGGER.info(e + " "));
        OptionalInt max = Arrays.stream(usingBuilder)
                .mapToInt(Integer::intValue)
                .max();
        LOGGER.info("\nmax = " + max.getAsInt());
        OptionalInt min = Arrays.stream(usingBuilder)
                .mapToInt(Integer::intValue)
                .min();
        LOGGER.info("\nmin = " + min.getAsInt());
    }

    public void calculate() {
        printAverage();
        printSum();
        printMinMax();
    }
}
