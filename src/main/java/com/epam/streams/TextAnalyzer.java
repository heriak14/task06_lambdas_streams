package com.epam.streams;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TextAnalyzer {
    private static final Logger LOG = LogManager.getLogger();
    private static final Scanner SCAN = new Scanner(System.in);
    private String text;

    public TextAnalyzer() {
        LOG.info("Enter some text:\n");
        readText();
    }

    private void readText() {
        text = "";
        String line;
        while (!(line = SCAN.nextLine()).isEmpty()) {
            text += line;
        }
    }

    private Stream<String> deletePunctualStream() {
        return Pattern.compile("[.,!?; ]+").splitAsStream(text);
    }

    private String getUniqueWords() {
        return deletePunctualStream().distinct().sorted().collect(Collectors.joining(", ", "{ ", "}"));
    }

    private Map<String, Long> getWordsCount() {
        return deletePunctualStream()
                .collect(Collectors.groupingBy(String::toString, Collectors.counting()));
    }

    private Map<Character, Long> getSymbolsCount() {
        return text.chars()
                .mapToObj(i -> (char) i)
                .filter(c -> c.toString().equals(c.toString().toLowerCase()))
                .collect(Collectors.groupingBy(Character::charValue, Collectors.counting()));
    }

    public void analyze() {
        LOG.info("Number of unique words: " + deletePunctualStream().distinct().count() + "\n");
        LOG.info("Sorted list of unique words: " + getUniqueWords() + "\n");
        LOG.info("\"" + text + "\"" + " -> \n");
        LOG.info(getWordsCount() + "\n");
        LOG.info(this::getSymbolsCount);
    }
}
