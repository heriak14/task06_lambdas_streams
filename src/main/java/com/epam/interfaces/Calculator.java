package com.epam.interfaces;

@FunctionalInterface
public interface Calculator {
    int calculate(int a, int b, int c);
}
