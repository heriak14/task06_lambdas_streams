package com.epam.interfaces;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class CalculatorImpl {
    private static final Scanner SCANNER = new Scanner(System.in);
    private static final Logger LOGGER = LogManager.getLogger();
    private Calculator maxCalc;
    private Calculator avgCalc;

    public CalculatorImpl() {
        maxCalc = (a, b, c) -> {
            int max = Integer.max(a, b);
            return Integer.max(max, c);
        };
        avgCalc = (a, b, c) -> (a + b + c) / 3;
    }

    public void calculate() {
        LOGGER.info("Enter 3 integers: ");
        int a = SCANNER.nextInt();
        int b = SCANNER.nextInt();
        int c = SCANNER.nextInt();
        LOGGER.info("Max of " + a + ", " + b + ", " + c + " = " + maxCalc.calculate(a, b, c) + "\n");
        LOGGER.info("Average of " + a + ", " + b + ", " + c + " = " + avgCalc.calculate(a, b, c) + "\n");
    }
}
