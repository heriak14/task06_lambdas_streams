package com.epam.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Executor {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final Scanner SCANNER = new Scanner(System.in, "UTF-8");
    private Map<String, String> menu;
    private Map<String, Command> commandMenu;

    public Executor() {
        menu = new LinkedHashMap<>();
        menu.put("1", "To lower case");
        menu.put("2", "To upper case");
        menu.put("3", "Split string");
        menu.put("4", "Replace capital with @");

        commandMenu = new LinkedHashMap<>();
        commandMenu.put("1", s -> LOGGER.trace(s.toLowerCase()));
        commandMenu.put("2", this::toUpperCase);
        commandMenu.put("3", new Command() {
            @Override
            public void execute(String arg) {
                Arrays.stream(arg.split(" ")).forEach(s -> LOGGER.info(s + "\n"));
            }
        });
        commandMenu.put("4", new CommandImpl());
    }

    private class CommandImpl implements Command {
        public void execute(String s) {
            LOGGER.trace(s.replaceAll("[A-Z]", "@") + "\n");
        }
    }

    private void toUpperCase(String s) {
        LOGGER.trace(s.toUpperCase() + "\n");
    }

    public void chooseCommand() {
        menu.forEach((k, v) -> LOGGER.info(k + " - " + v + "\n"));
        LOGGER.trace("Enter number of command and string: ");
        String[] input = SCANNER.nextLine().split(" |-");
        commandMenu.get(input[0]).execute(input[1]);
    }
}
