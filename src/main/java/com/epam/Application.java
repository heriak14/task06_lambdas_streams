package com.epam;

import com.epam.command.Executor;
import com.epam.interfaces.Calculator;
import com.epam.interfaces.CalculatorImpl;
import com.epam.streams.StreamCalculator;
import com.epam.streams.TextAnalyzer;

import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        new CalculatorImpl().calculate();
        new Executor().chooseCommand();
        new StreamCalculator().calculate();
        new TextAnalyzer().analyze();
    }
}
